import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/modules/binding/add_product_binding.dart';
import 'package:goerstechintensiveassignment/modules/binding/detail_binding.dart';
import 'package:goerstechintensiveassignment/modules/binding/edit_product_binding.dart';
import 'package:goerstechintensiveassignment/modules/binding/home_binding.dart';
import 'package:goerstechintensiveassignment/modules/view/add_product_screen.dart';
import 'package:goerstechintensiveassignment/modules/view/detail_screen.dart';
import 'package:goerstechintensiveassignment/modules/view/edit_screen.dart';
import 'package:goerstechintensiveassignment/modules/view/home_screen.dart';
import 'package:goerstechintensiveassignment/router/router_page.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: homeScreenViewRoute, 
      page: () => HomeScreenView(),
      binding: HomeBinding()
    ),
    GetPage(
      name: detailScreenViewRoute, 
      page: () => DetailScreenView(),
      binding: DetailBinding()
    ),
    GetPage(
      name: addProductScreenViewRoute, 
      page: () => AddProductScreenView(),
      binding: AddProductBinding()
    ),
    GetPage(
      name: editProductScreenViewRoute, 
      page: () => EditProductScreenView(),
      binding: EditProductBinding()
    )
  ];
}