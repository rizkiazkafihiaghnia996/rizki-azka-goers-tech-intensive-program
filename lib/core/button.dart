import 'package:flutter/material.dart';
import 'package:goerstechintensiveassignment/core/colors.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';

class PrimaryButton extends StatelessWidget {
  final onPressed;
  final String text;
  final bool isDisabled;
  const PrimaryButton(
      {Key? key, this.onPressed, this.text = '', this.isDisabled = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: getPrimaryButtonStyle(isDisabled: isDisabled),
        onPressed: onPressed ?? onPressed,
        child: Text(
          text,
          style: buttonLg(
              color: isDisabled || onPressed == null
                  ? iconColorPrimary
                  : Colors.white),
        ));
  }
}

getPrimaryButtonStyle({isDisabled: false}) {
  return ButtonStyle(
      elevation: MaterialStateProperty.all(isDisabled ? 0 : 2),
      shadowColor: MaterialStateProperty.all(Colors.black),
      padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 12)),
      shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))),
      backgroundColor: MaterialStateProperty.all(
          isDisabled ? backgroundColorPrimary : Colors.black));
}

getCustomButtonStyle({color = contextOrange, borderColor = Colors.black}) {
  return ButtonStyle(
      elevation: MaterialStateProperty.all(1),
      shadowColor: MaterialStateProperty.all(Colors.black),
      padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 12)),
      shape: MaterialStateProperty.all(RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100),
          side: BorderSide(color: borderColor))),
      backgroundColor: MaterialStateProperty.all(color));
}
