import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/button.dart';
import 'package:goerstechintensiveassignment/core/colors.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';

class ConfirmationDialog extends StatelessWidget {
  final String title;
  final String content;
  final onConfirmation;
  const ConfirmationDialog(
      {Key? key,
      required this.title,
      required this.content,
      this.onConfirmation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsPadding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      title: Text(
        title,
        textAlign: TextAlign.center,
        style: h5(),
      ),
      content: Text(
        content,
        textAlign: TextAlign.center,
        style: bodyMd(color: textGrey),
      ),
      actions: [
        FractionallySizedBox(
          widthFactor: 0.485,
          child: ElevatedButton(
            onPressed: () {
              Get.back();
            },
            style: getCustomButtonStyle(color: Colors.white),
            child: Text(
              'No',
              style: buttonMd(color: Colors.black),
            ),
          ),
        ),
        FractionallySizedBox(
          widthFactor: 0.45,
          child: PrimaryButton(
            text: 'Yes',
            onPressed: onConfirmation ?? null,
          ),
        ),
      ],
    );
  }
}
