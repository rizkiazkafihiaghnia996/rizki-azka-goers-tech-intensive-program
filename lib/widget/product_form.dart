import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:goerstechintensiveassignment/core/colors.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';

class ProductForm extends StatefulWidget {
  final Key formKey;
  final AutovalidateMode autovalidateMode;
  final TextEditingController controller;
  final String hintText;
  final String? Function(String?) validator;
  final Color focusedColor;
  final TextInputType keyboardType;
  final int maxLines;

  const ProductForm({
    Key? key, 
    required this.formKey, 
    required this.autovalidateMode,
    required this.controller,
    required this.hintText,
    required this.validator, 
    this.focusedColor = Colors.black,
    this.keyboardType = TextInputType.text,
    this.maxLines = 1
  }) : super(key: key);

  @override
  State<ProductForm> createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      autovalidateMode: widget.autovalidateMode,
      child: TextFormField(
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        maxLines: widget.maxLines,
        decoration: InputDecoration(
          hintText: widget.hintText,
          hintStyle: h5(color: textGrey),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: widget.focusedColor, 
              width: 2.0
            )
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: widget.focusedColor, width: 2.0
            )
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: contextRed, 
              width: 2.0
            )
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: contextRed, 
              width: 2.0
            )
          )
        ),
        validator: widget.validator
      )
    );
  }
}
