import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:goerstechintensiveassignment/core/colors.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';

Widget uploadProgress(UploadTask? uploadTask) => StreamBuilder<TaskSnapshot>(
  stream: uploadTask!.snapshotEvents,
  builder: (context, snapshot) {
    if (snapshot.hasData) {
      final data = snapshot.data!;
      double progress = data.bytesTransferred / data.totalBytes;
        
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18),
        ),
        title: Text(
          'Uploading Image...',
          style: h5(),
        ),
        content: SizedBox(
          height: 50,
          child: Stack(
            fit: StackFit.expand,
            children: [
              LinearProgressIndicator(
                value: progress,
                backgroundColor: Colors.white,
                color: Colors.black,
              ),
              Center(
                child: Text(
                  '${(100 * progress).roundToDouble()}%',
                  style: h5(color: Colors.white),
                ),
              )
            ],
          ),
        )
      );
    } else {
      return const SizedBox.shrink();
    }
  },
);

Widget uploadComplete() => AlertDialog(
  title: Text(
    'Uploading Image...',
    style: h5(),
  ),
  content: Text(
    'Upload Complete!',
    style: bodyMd(color: textGrey),
    textAlign: TextAlign.center,
  ),
);
