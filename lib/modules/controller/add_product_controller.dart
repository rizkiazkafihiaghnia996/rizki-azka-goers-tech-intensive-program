import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';

class AddProductController extends GetxController {
  TextEditingController productNameController = TextEditingController();
  TextEditingController productPriceController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  
  final productNameFormKey = GlobalKey<FormState>();
  final productPriceFormKey = GlobalKey<FormState>();
  final productDescriptionFormKey = GlobalKey<FormState>();

  var autoValidateProductName = AutovalidateMode.disabled;
  var autoValidateProductPrice = AutovalidateMode.disabled;
  var autoValidateProductDescription = AutovalidateMode.disabled;

  Rxn<PlatformFile> _selectedImage = Rxn<PlatformFile>();
  RxString _imageUrl = ''.obs;
  Rxn<UploadTask> _uploadTask = Rxn<UploadTask>();

  PlatformFile? get selectedImage => _selectedImage.value;
  String get imageUrl => _imageUrl.value;
  UploadTask? get uploadTask => _uploadTask.value;

  set selectedImage(PlatformFile? selectedImage) =>
      this._selectedImage.value = selectedImage;
  set imageUrl(String imageUrl) =>
      this._imageUrl.value = imageUrl;
  set uploadTask(UploadTask? uploadTask) =>
      this._uploadTask.value = uploadTask;

  Future selectImage() async {
    final result = await FilePicker.platform.pickFiles();
    if (result == null) return;

    selectedImage = result.files.first;
    print(selectedImage!.path);
  }

  Future uploadImage() async {
    final path = 'products_images/${productNameController.text}/${selectedImage!.name}';
    final file = File(selectedImage!.path.toString());

    final ref = FirebaseStorage.instance.ref().child(path);
    uploadTask = ref.putFile(file);

    final snapshot = await uploadTask!.whenComplete(() {});
    imageUrl = await snapshot.ref.getDownloadURL();
    print('Product Image URL: $imageUrl');

    uploadTask = null;
  }

  Future<void> addProduct(
    String productImageUrl, 
    String productName, 
    String productPrice, 
    String productDesc
  ) async {
    DocumentReference products = FirebaseFirestore.instance.collection('products').doc();
    
    var product = Product(
      id: products.id,
      imageUrl: productImageUrl, 
      productName: productName, 
      productPrice: '\$ $productPrice', 
      productDesc: productDesc
    );
    
    final json = product.toJson();
    await products.set(json);
  }
}