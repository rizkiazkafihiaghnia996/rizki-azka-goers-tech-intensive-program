import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';
import 'package:goerstechintensiveassignment/modules/controller/edit_product_controller.dart';
import 'package:goerstechintensiveassignment/router/router_page.dart';
import 'package:goerstechintensiveassignment/widget/confirmation_dialog.dart';
import 'package:goerstechintensiveassignment/widget/custom_snackbar.dart';

class DetailController extends GetxController {
  FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  EditProductController editProductController = Get.put(EditProductController());
  
  RxString _productId = ''.obs;
  RxString _productTitle = ''.obs;
  RxString _productImageUrl = ''.obs;
  RxString _productPrice = ''.obs;
  RxString _productDesc = ''.obs;

  String get productId => _productId.value;
  String get productTitle => _productTitle.value;
  String get productImageUrl => _productImageUrl.value;
  String get productPrice => _productPrice.value;
  String get productDesc => _productDesc.value;


  set productId(String productId) =>
      this._productId.value = productId;
  set productTitle(String productTitle) =>
      this._productTitle.value = productTitle;
  set productImageUrl(String productImageUrl) =>
      this._productImageUrl.value = productImageUrl;
  set productPrice(String productPrice) =>
      this._productPrice.value = productPrice;
  set productDesc(String productDesc) =>
      this._productDesc.value = productDesc;

  Stream<Product> getProductDetail(String id) => firebaseFirestore.collection('products')
      .doc(id)
      .snapshots()
      .map((snapshot) => Product.fromJson(snapshot.data()!));

  Widget buildProductDetail(Product product, BuildContext context) => Stack(
    children: [
      CachedNetworkImage(
        imageUrl: product.imageUrl,
        fit: BoxFit.fill,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 2,
        fadeInDuration: const Duration(milliseconds: 300),
        errorWidget: (context, url, error) => const Icon(Icons.error),
        placeholder: (context, url) => const SpinKitThreeBounce(
          color: Colors.black,
          size: 18
        )
      ),
      Container(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: MediaQuery.of(context).size.height / 1.8,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(16, 24, 16, 24),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(32),
              topRight: Radius.circular(32)
            ),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 3)
              )
            ]
          ),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  product.productName,
                  style: h4(),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'Price: ${product.productPrice}',
                  style: h5(fontWeight: FontWeight.normal),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 25),
                Text(
                  product.productDesc,
                  style: h5(fontWeight: FontWeight.normal),
                  textAlign: TextAlign.justify,
                )
              ],
            ),
          ),
        ),
      )
    ],
  );

  void handleClick(int item) {
    switch(item) {
      case 0:
        handleEditProduct();
        break;
      case 1:
        deleteProductConfirmation();
        break;
    }
  }

  void handleEditProduct() {
    editProductController.productId = productId;
    editProductController.originalImageUrl = productImageUrl;
    editProductController.productNameController.text = productTitle;
    editProductController.productPriceController.text = productPrice.replaceAll(RegExp('[^A-Za-z0-9]'), '');
    editProductController.productDescriptionController.text = productDesc;
    Get.toNamed(editProductScreenViewRoute);
  }

  deleteProductConfirmation() {
    Get.dialog(ConfirmationDialog(
      title: "Hold up!", 
      content: "Are you sure you want to delete this product? Once deleted, you won't be able to restore it",
      onConfirmation: () {
        handleDeleteProduct();
      },
    ));
  }

  void handleDeleteProduct() {
    final product = firebaseFirestore.collection('products').doc(productId);
    product.delete();
    Get.offAllNamed(homeScreenViewRoute);
    customSnackbar('Success!', 'Product has been deleted');
  }
}