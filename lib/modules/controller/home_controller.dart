import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';
import 'package:goerstechintensiveassignment/modules/controller/detail_controller.dart';
import 'package:goerstechintensiveassignment/router/router_page.dart';

class HomeController extends GetxController {
  DetailController detailController = Get.put(DetailController());
  FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<Product>> getProducts() => firebaseFirestore.collection('products')
      .snapshots()
      .map((snapshot) => 
          snapshot.docs.map((doc) => Product.fromJson(doc.data())).toList());

  Widget buildProduct(Product product) => InkWell(
    onTap: () {
      detailController.productId = product.id;
      detailController.productTitle = product.productName;
      detailController.productImageUrl = product.imageUrl;
      detailController.productPrice = product.productPrice;
      detailController.productDesc = product.productDesc;
      Get.toNamed(detailScreenViewRoute);
    },
    child: Container(
      width: Get.mediaQuery.size.width,
      height: Get.mediaQuery.size.height / 6,
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3)
          )
        ]
      ),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: CachedNetworkImage(
              imageUrl: product.imageUrl,
              fit: BoxFit.cover,
              width: Get.mediaQuery.size.width / 5,
              height: Get.mediaQuery.size.height / 8,
              fadeInDuration: const Duration(milliseconds: 300),
              errorWidget: (context, url, error) => const Icon(Icons.error),
              placeholder: (context, url) => const SpinKitThreeBounce(
                color: Colors.black,
                size: 18
              )
            ),
          ),
          const SizedBox(width: 12),
          Expanded(
            flex: 4,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.productName,
                  style: h5(),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'Price: ${product.productPrice}',
                  style: h6(fontWeight: FontWeight.normal),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}