import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';

class EditProductController extends GetxController {
  TextEditingController productNameController = TextEditingController();
  TextEditingController productPriceController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  
  final productNameFormKey = GlobalKey<FormState>();
  final productPriceFormKey = GlobalKey<FormState>();
  final productDescriptionFormKey = GlobalKey<FormState>();

  var autoValidateProductName = AutovalidateMode.disabled;
  var autoValidateProductPrice = AutovalidateMode.disabled;
  var autoValidateProductDescription = AutovalidateMode.disabled;

  Rxn<PlatformFile> _selectedImage = Rxn<PlatformFile>();
  RxString _imageUrl = ''.obs;
  RxString _originalImageUrl = ''.obs;
  Rxn<UploadTask> _uploadTask = Rxn<UploadTask>();
   RxString _productId = ''.obs;

  PlatformFile? get selectedImage => _selectedImage.value;
  String get imageUrl => _imageUrl.value;
  String get originalImageUrl => _originalImageUrl.value;
  UploadTask? get uploadTask => _uploadTask.value;
  String get productId => _productId.value;

  set selectedImage(PlatformFile? selectedImage) =>
      this._selectedImage.value = selectedImage;
  set imageUrl(String imageUrl) =>
      this._imageUrl.value = imageUrl;
  set originalImageUrl(String originalImageUrl) =>
      this._originalImageUrl.value = originalImageUrl;
  set uploadTask(UploadTask? uploadTask) =>
      this._uploadTask.value = uploadTask;
  set productId(String productId) =>
      this._productId.value = productId;

  Future selectImage() async {
    final result = await FilePicker.platform.pickFiles();
    if (result == null) return;

    selectedImage = result.files.first;
    print(selectedImage!.path);
  }

  Future uploadImage() async {
    final path = 'products_images/${productNameController.text}/${selectedImage!.name}';
    final file = File(selectedImage!.path.toString());

    final ref = FirebaseStorage.instance.ref().child(path);
    uploadTask = ref.putFile(file);

    final snapshot = await uploadTask!.whenComplete(() {});
    imageUrl = await snapshot.ref.getDownloadURL();
    print('Product Image URL: $imageUrl');
  }

  Future<void> updateProduct(
    String productImageUrl, 
    String productName, 
    String productPrice, 
    String productDesc
  ) async {
    DocumentReference products = FirebaseFirestore.instance.collection('products').doc(productId);
    print('ID: $productId');
    var json = {
      'image_url': productImageUrl, 
      'product_name': productName, 
      'product_price': '\$ $productPrice', 
      'product_desc': productDesc
    };

    print(json);
    await products.update(json);
  }
}