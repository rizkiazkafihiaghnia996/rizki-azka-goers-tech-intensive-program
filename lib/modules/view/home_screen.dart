import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';
import 'package:goerstechintensiveassignment/modules/controller/home_controller.dart';
import 'package:goerstechintensiveassignment/router/router_page.dart';
import 'package:goerstechintensiveassignment/widget/skeleton_loader.dart';

class HomeScreenView extends StatefulWidget {
  const HomeScreenView({Key? key}) : super(key: key);

  @override
  State<HomeScreenView> createState() => _HomeScreenViewState();
}

class _HomeScreenViewState extends State<HomeScreenView> {
  HomeController controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My Products',
          style: h4(color: Colors.black),
        ),
        elevation: 16,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              Get.toNamed(addProductScreenViewRoute);
            }, 
            icon: const Icon(
              Icons.add,
              size: 25,
              color: Colors.black,
            )
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.all(16),
          child: StreamBuilder<List<Product>>(
            stream: controller.getProducts(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Column(
                  children: [
                    const Icon(
                      Icons.error,
                      size: 50,
                    ),
                    Text(
                      'Something went Wrong! \n${snapshot.error}',
                      style: h6(),
                    )
                  ],
                );
              } else if (snapshot.hasData) {
                final products = snapshot.data!;

                return ListView(
                  physics: const BouncingScrollPhysics(),
                  children: products.map(controller.buildProduct).toList(),
                );
              } else if (!snapshot.hasData){
                return SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: const [
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                    ]
                  ),
                );
              } else if (snapshot.data == null){
                return Container(
                  padding: EdgeInsets.only(top: Get.mediaQuery.size.height / 7),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/img/not_found.png',
                          width: MediaQuery.of(context).size.width / 2,
                          height: MediaQuery.of(context).size.height / 6,
                        ),
                        const SizedBox(height: 12),
                        Text(
                          'No products found. \nAdd one by clicking the "+" icon above.',
                          textAlign: TextAlign.center,
                          style: h6(),
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return const SizedBox.shrink();
              }
            }
          ),
        ) 
      ),
    );
  }
}