import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/modules/controller/edit_product_controller.dart';
import 'package:goerstechintensiveassignment/router/router_page.dart';
import 'package:goerstechintensiveassignment/widget/confirmation_dialog.dart';
import 'package:goerstechintensiveassignment/widget/custom_snackbar.dart';
import 'package:goerstechintensiveassignment/widget/loader_dialog.dart';
import 'package:goerstechintensiveassignment/widget/primary_button_navigation_bar.dart';
import 'package:goerstechintensiveassignment/widget/product_form.dart';
import 'package:goerstechintensiveassignment/widget/upload_progress.dart';

class EditProductScreenView extends StatefulWidget {
  const EditProductScreenView({Key? key}) : super(key: key);

  @override
  State<EditProductScreenView> createState() => _EditProductScreenViewState();
}

class _EditProductScreenViewState extends State<EditProductScreenView> {
  EditProductController controller = Get.find<EditProductController>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.dialog(ConfirmationDialog(
          title: "Hold up!", 
          content: "Are you sure you want to leave this page? All changes you made will be discarded",
          onConfirmation: () {
            Navigator.popUntil(
              context, 
              ModalRoute.withName(detailScreenViewRoute)
            );
          },
        ));
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Edit Product',
            style: h4(color: Colors.black),
          ),
          iconTheme: const IconThemeData(color: Colors.black),
          elevation: 16,
          backgroundColor: Colors.white,
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                Stack(
                  children: [
                    Obx(() => controller.selectedImage == null ? CachedNetworkImage(
                      imageUrl: controller.originalImageUrl,
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height / 3,
                      width: MediaQuery.of(context).size.width,
                      fadeInDuration: const Duration(milliseconds: 300),
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                      placeholder: (context, url) => const SpinKitThreeBounce(
                        color: Colors.black,
                        size: 18
                      )
                    ) : Image.file(
                      File(controller.selectedImage!.path.toString()),
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height / 3,
                      width: MediaQuery.of(context).size.width
                    )),
                    Positioned(
                      top: 8,
                      right: 8,
                      child: TextButton(
                        onPressed: () {
                          controller.selectImage();
                        },
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
                          minimumSize: MaterialStateProperty.all(Size(60, 30)),
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: const BorderSide(
                                color:Colors.black
                              )
                            )
                          ),
                          backgroundColor: MaterialStateProperty.all(Colors.white)
                        ),
                        child: Text(
                          'Edit',
                          style: buttonSm(color: Colors.black)
                        )
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 12),
                ProductForm(
                  formKey: controller.productNameFormKey, 
                  autovalidateMode: controller.autoValidateProductName, 
                  controller: controller.productNameController, 
                  hintText: 'Product Name', 
                  validator: (value) {
                    if(value!.isEmpty) {
                      return 'Product name cannot be empty';
                    }
                  }
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        '\$ (USD)',
                        style: h5(),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: ProductForm(
                        formKey: controller.productPriceFormKey, 
                        autovalidateMode: controller.autoValidateProductPrice, 
                        controller: controller.productPriceController, 
                        hintText: 'Price',
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if(value!.isEmpty) {
                            return 'Price cannot be empty';
                          }
                        }
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                ProductForm(
                  formKey: controller.productDescriptionFormKey, 
                  autovalidateMode: controller.autoValidateProductDescription, 
                  controller: controller.productDescriptionController, 
                  hintText: 'Description',
                  maxLines: 5,
                  validator: (value) {
                    if(value!.isEmpty) {
                      return 'Description cannot be empty';
                    }
                  }
                )
              ],
            ),
          ),
        ),
        bottomNavigationBar: PrimaryButtonBottomNavigationBar(
          onPressed: () async {
            final isNameValid = controller.productNameFormKey.currentState!.validate();
            final isPriceValid = controller.productPriceFormKey.currentState!.validate();
            final isDescValid = controller.productDescriptionFormKey.currentState!.validate();
    
            if (controller.selectedImage != null) {
              Get.dialog(
                Obx(() => controller.uploadTask != null ? 
                  uploadProgress(controller.uploadTask) : 
                  uploadComplete()
                ),
                barrierDismissible: true
              );
              await controller.uploadImage();
              loaderDialog(
                const SpinKitRing(color: Colors.black), 
                'Please wait for a moment'
              );
              await controller.updateProduct(
                controller.imageUrl, 
                controller.productNameController.text, 
                controller.productPriceController.text, 
                controller.productDescriptionController.text,
              );
              Get.offNamedUntil(homeScreenViewRoute, (route) => false);
              customSnackbar('Success!', 'New product has been updated');
            } else if (controller.selectedImage == null) {
              loaderDialog(
                const SpinKitRing(color: Colors.black), 
                'Please wait for a moment'
              );
              await controller.updateProduct(
                controller.originalImageUrl, 
                controller.productNameController.text, 
                controller.productPriceController.text, 
                controller.productDescriptionController.text
              );
              Get.offNamedUntil(homeScreenViewRoute, (route) => false);
              customSnackbar('Success!', 'Product has been updated');
            } else if (!isNameValid) {
              controller.autoValidateProductName = AutovalidateMode.always;
            } else if (!isPriceValid) {
              controller.autoValidateProductPrice = AutovalidateMode.always;
            } else if (!isDescValid) {
              controller.autoValidateProductDescription = AutovalidateMode.always;
            } 
          },
          title: 'Update Product',
        ),
      ),
    );
  }
}