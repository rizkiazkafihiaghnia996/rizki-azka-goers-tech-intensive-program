import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/core/fonts.dart';
import 'package:goerstechintensiveassignment/data/models/product_response.dart';
import 'package:goerstechintensiveassignment/modules/controller/detail_controller.dart';
import 'package:goerstechintensiveassignment/widget/skeleton_loader.dart';

class DetailScreenView extends StatefulWidget {
  const DetailScreenView({Key? key}) : super(key: key);

  @override
  State<DetailScreenView> createState() => _DetailScreenViewState();
}

class _DetailScreenViewState extends State<DetailScreenView> {
  DetailController controller = Get.find<DetailController>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Text(
            controller.productTitle,
            style: h4(color: Colors.black,),
          ),
          centerTitle: true,
          iconTheme: const IconThemeData(color: Colors.black),
          elevation: 0,
          backgroundColor: Colors.transparent,
          actions: [
            PopupMenuButton<int>(
              onSelected: (item) => controller.handleClick(item),
              itemBuilder: (context) => [
                const PopupMenuItem<int>(value: 0, child: Text('Edit Product')),
                const PopupMenuItem<int>(value: 1, child: Text('Delete Product'))
              ]
            )
          ]
        ),
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: StreamBuilder<Product>(
            stream: controller.getProductDetail(controller.productId),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Column(
                  children: [
                    const Icon(
                      Icons.error,
                      size: 50,
                    ),
                    Text(
                      'Something went Wrong! \n${snapshot.error}',
                      style: h6(),
                    )
                  ],
                );
              } else if(snapshot.hasData) {
                final productDetail = snapshot.data;
        
                return controller.buildProductDetail(productDetail!, context);
              } else if (!snapshot.hasData){
                return SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: const [
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                      SkeletonLoader(),
                    ]
                  ),
                );
              } else {
                return const SizedBox.shrink();
              }
            },
          ),
        ),
      ),
    );
  }
}