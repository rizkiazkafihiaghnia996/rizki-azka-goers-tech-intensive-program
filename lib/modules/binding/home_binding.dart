import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/modules/controller/home_controller.dart';

class HomeBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
  }
}