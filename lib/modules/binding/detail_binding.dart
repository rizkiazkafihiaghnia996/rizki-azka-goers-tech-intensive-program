import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/modules/controller/detail_controller.dart';

class DetailBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<DetailController>(() => DetailController());
  }
}