import 'package:get/get.dart';
import 'package:goerstechintensiveassignment/modules/controller/add_product_controller.dart';

class AddProductBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<AddProductController>(() => AddProductController());
  }
}