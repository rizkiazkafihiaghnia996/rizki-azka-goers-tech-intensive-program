class Product {
  final String id;
  final String imageUrl;
  final String productName;
  final String productPrice;
  final String productDesc;

  Product({
    required this.id,
    required this.imageUrl, 
    required this.productName, 
    required this.productPrice, 
    required this.productDesc, 
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'image_url': imageUrl,
    'product_name': productName,
    'product_price': productPrice,
    'product_desc': productDesc
  };

  static Product fromJson(Map<String, dynamic> json) => Product(
    id: json['id'],
    imageUrl: json['image_url'],
    productName: json['product_name'],
    productPrice: json['product_price'],
    productDesc: json['product_desc']
  );
}